/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id User's unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
class User {
    public vorname: string;
    public nachname: string;
    public adresse: string;
    public plz: number;
    public email: string;
    public passwort: string;
    public anrede: string;
    id: number;
    public typ: number;

    constructor (anrede: string, id: number, vorname: string, nachname: string, adresse: string, plz: number, email: string, passwort: string, typ: number) {
        this.anrede=anrede;
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.adresse = adresse;
        this.plz = plz;
        this.email = email;
        this.passwort = passwort;
        this.typ = typ;
    }
}

enum Typ {
    Admin = 1,
    Unternehmen = 2,
    Kunde = 3,
}


let inputvorname: JQuery;
let inputnachname: JQuery;
let inputadresse: JQuery;
let inputplz: JQuery;
let inputemail: JQuery;
let inputpasswort: JQuery;
let inputKunde: JQuery;
let inputUnternehmen: JQuery;
let inputpwiederholen: JQuery;
let inputregi: JQuery;

let inputanrede;
let user: User;
//let userList: User[];

let editAnrede: JQuery;
let editName: JQuery;
let editNachname: JQuery;
let editAddress: JQuery;
let editPlz: JQuery;
let editPasswort: JQuery;
let editEmail: JQuery;
let editProfil: JQuery;
let editUserForm: JQuery;
let editModal: JQuery;

let vornameInput: JQuery;
let nachnameInput: JQuery;
let idHiddenInput: JQuery;
let adresseInput: JQuery;
let plzInput: JQuery;

$ ( () => {

    inputregi=$("#inputRegi");
    inputKunde = $ ("#radioKunde");
    inputUnternehmen = $("#radioUnternehmen");
    inputvorname = $ ("#inputName");
    inputnachname = $ ("#inputNachname");
    inputadresse = $ ("#inputAddress");
    inputplz = $ ("#inputPlz");
    inputemail = $ ("#inputEmail");
    inputpasswort = $ ("#inputPassword");
    inputpwiederholen = $("#inputPwiederholen");
    editUserForm= $('#edit-user-form');
    editModal =$('#edit-user-modal');
    let tableUser: JQuery = $("#tableUser");

    vornameInput= $('#edit-first-name-input');
    nachnameInput = $('#edit-last-name-input');
    idHiddenInput = $('#edit-id-input');
    adresseInput = $('#edit-adresse');
    plzInput= $('#edit-plz');


    editAnrede= $("#editAnrede");
    editName = $ ("#editName");
    editNachname = $("#editNachname");
    editAddress = $("#editAddress");
    editPlz = $("#editPlz");
    editPasswort = $("#editPassword");
    editEmail = $("#editEmail");
    editProfil = $("#editProfil");


    checkLogin();

    inputregi.on("submit", addUser);
    $ ("#loginform").on("submit", loginUser);
    $("#profilcontent").hide();
    $("#logoutform").hide();
    $('#adminContent').hide();

    $('#profil').on('submit', editUser);
    $("#logoutform").on("submit", logout);
    $('#deleteProfil').on('click', deleteUser);
    editUserForm.on('submit', adminEditUser);

    tableUser.on('click', '.btnedit', openEditUserModal); // Click listener for edit button
    tableUser.on('click', '.dltedit', adminDeleteUser); // Click listener for delete button

});


function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const vorname: string = inputvorname.val().toString().trim();
    const nachname: string = inputnachname.val().toString().trim();
    const adresse: string = inputadresse.val().toString().trim();
    const plz: number = inputplz.val() as number;
    const email: string = inputemail.val().toString().trim();
    const passwort: string = inputpasswort.val().toString().trim();
    const passwortw: string = $ ("#inputPwiederholen").val().toString().trim();
    const typ: number = $("#radioTyp").find('input[name="customRadioInline1"]:checked').val() as number;
    const anrede: string = $( "#select").find('option[name="opt"]:selected').val() as string;

    // Check if all required fields are filled in
    if (anrede && vorname && nachname && adresse && plz && email && passwort && passwortw && typ) {
        $.ajax({
            url: '/create',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                vorname,
                nachname,
                adresse,
                plz,
                email,
                passwort,
                passwortw,
                typ,
                anrede,
            }),
            contentType: 'application/json',
            success: (res) => {
                // Update local user list
                updateUserList();
                inputregi.trigger('reset');
                renderMessage(res.message);
            },
            error: (response) => {
                //checkErrorResponse(response);
            },
        });
    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Bitte füllen Sie alle Felder aus');
    }
}

function checkLogin() {

    // Perform ajax request to check if user is logged in
    $.ajax({
        url: '/login',
        type: 'GET',
        dataType: 'json',
        success: (response) => {
           loginSuccess(response.user);
            getUser(response.user.id);
            updateUserList();
            renderMessage('Sitzung aktiv');
        },
        error: (response) => {
            checkErrorResponse(response);
        },
    });
}

function loginSuccess(user: User) {
    $('#loginContent').hide();
    $('#logoutform').show();
    $('#regiform').hide();
    console.log('########'+ user.typ);

    if(user.typ == Typ.Kunde) {
        $('#profilcontent').show();
        $('#unternehmenansicht').hide();
    } else if(user.typ == Typ.Unternehmen) {
        $('#profilcontent').show();
        $('#unternehmenansicht').show();
    } else if(user.typ == Typ.Admin){
        $('#adminContent').show();
        $('#profilcontent').hide();
        $('#unternehmenansicht').hide();
    }
}

function loginUser(event) {
    console.log("Log dich ein");
    event.preventDefault();
    const logoutForm: JQuery = $('#logoutform');
    const email: string = $("#logEmail").val() as string;
    const passwort: string = $("#logPassword").val() as string;
    let loginForm: JQuery = $ ("#loginform");
    let regiform: JQuery = $ ("#inputRegi");
    let profilform: JQuery = $ ("#profil");
    if (email && passwort) {
        // Perform ajax request to log user in
        $.ajax({
            url: '/login',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                passwort,
                email,
            }),
            contentType: 'application/json',
            success: (response) => {
                loginForm.trigger('reset');
                renderMessage("Sie haben sich eingeloggt");
                checkLogin();
                getUser(response.id);
                updateUserList();
            },
            error: (response) => {
                checkErrorResponse(response);
                //renderMessage("...Sind Sie nicht registriert?");
            },
        });
    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Bitte füllen/wählen Sie alle Felder aus');
    }
}

function renderUserList(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#tableUser');

    // Delete the old table of users from the DOM
    userTableBody.empty();
    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.anrede}</td>
                <td>${user.id}</td>
                <td>${user.vorname}</td>
                <td>${user.nachname}</td>
                <td>${user.adresse}</td>
                <td>${user.plz}</td>
                <td>${user.email}</td>
                <td>${user.passwort}</td>
                <td>${user.typ}</td>
                <td>
                    <button class="btn btn-outline-dark btn-sm btnedit" data-user-id="${user.id}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm dltedit" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);

        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

function getUser(userId){
    $.ajax({
        url: '/user/' + userId,
            type: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#user_id').val(userId);
                editAnrede.val(data.anrede);
                editName.val(data.vorname);
                editNachname.val(data.nachname);
                editAddress.val(data.adresse);
                editPlz.val(data.plz);
                editEmail.val(data.email);
                editPasswort.val(data.passwort);
                editProfil.val(data.id);
                console.log("----"+data.plz);
        },
            error: (response) => {
            //checkErrorResponse(response);
        },
    });
}

function editUser(event) {
    event.preventDefault();
    let userId = $('#user_id').val();
    let vorname = editName.val();
    let nachname = editNachname.val();
    let adresse = editAddress.val();
    let plz = editPlz.val();
    //let email = editEmail.val();
    let passwort = editPasswort;

        $.ajax({
            url: '/user/' + userId,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({
                vorname,
                nachname,
                adresse,
                plz,
                passwort,
            }),
            contentType: 'application/json',
            success: (res) => {
                let profilform: JQuery = $ ("#profil");
                profilform.trigger('reset');
                getUser(userId);
                updateUserList();
                renderMessage(res.message);
            },
            error: (response) => {
                console.log(response);
                checkErrorResponse(response);
            },
        });

}

function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    //const editFirstNameInput: JQuery = $('#edit-first-name-input');
    //const editLastNameInput: JQuery = $('#edit-last-name-input');

    $.ajax({
        url: '/user/' + userId,
        type: 'GET',
        dataType: 'json',
        success: (response) => {
            // Fill in edit fields in modal
            editIdInput.val(response.id);
            //editFirstNameInput.val(response.vorname);
            //editLastNameInput.val(response.nachname);
            // Show modal
            vornameInput.val(response.vorname);
            nachnameInput.val(response.nachname);
            adresseInput.val(response.adresse);
            plzInput.val(response.plz);
            editUserModal.modal('show');
        },
        error: (response) => {
            checkErrorResponse(response);
        },
    });
}

function adminEditUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');


    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const vorname: string = vornameInput.val().toString().trim();
    const nachname: string = nachnameInput.val().toString().trim();
    const adresse:string = adresseInput.val().toString().trim();
    const plz: number = Number(plzInput.val().toString().trim());

    // Check if all required fields are filled in
    if (vorname && nachname) {
        $.ajax({
            url: '/user/' + userId,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({
                vorname,
                nachname,
                adresse,
                plz,
            }),
            contentType: 'application/json',
            success: () => {
                // Update local user list
                updateUserList();
                editUserForm.trigger('reset');
            },
            error: (response) => {
                checkErrorResponse(response);
            },
        });
    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Bitte füllen Sie alle Felder aus');
    }

    editModal.modal('hide');
}

function adminDeleteUser(event) {
    const userId: number = $(event.currentTarget).data('user-id');

    // Perform ajax request to log out user
    $.ajax({
        url: '/user/' + userId,
        type: 'DELETE',
        dataType: 'json',
        success: () => {
            // Get new user list from server
            updateUserList();
            renderMessage("Benutzer wurde erfolgreich gelöscht");
        },
        error: (response) => {
            checkErrorResponse(response);
        },
    });
}

function checkErrorResponse(error): void {

    renderMessage(error.responseJSON.message);

}

function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);
    messageWindow.append(newAlert);

    messageWindow.append(newAlert);

    setTimeout(() => {
        newAlert.alert('close');
    }, 5000);
}

function updateUserList() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/users',
        type: 'GET',
        dataType: 'json',
        success: (response) => {

            renderUserList(response.userList);
            //renderMessage(response.message);
        },
        error: (response) => {
            checkErrorResponse(response);
        },
    });
}

function deleteUser(event) {
    let userId = $('#user_id').val();


    // Perform ajax request to log out user
    $.ajax({
        url: '/user/' + userId,
        type: 'DELETE',
        dataType: 'json',
        success: (res) => {
            // Get new user list from server
            renderMessage(res.message);
            $('#profilcontent').hide();
            $('#unternehmenansicht').hide();
            $('#loginContent').show();
        },
        error: (response) => {
            checkErrorResponse(response);
        },
    });
}


function logout() {
    // Define JQuery HTML objects
    const loginForm: JQuery = $('#loginContent');
    const logoutForm: JQuery = $('#logout');
    const contentArea: JQuery = $('#profilcontent');
    //const contentArea: JQuery = $('#profildaten');

    // Perform ajax request to log out user
    $.ajax({
        url: '/logout',
        type: 'POST',
        dataType: 'json',
        success: (res) => {
            logoutForm.fadeOut(() => loginForm.fadeIn());
            contentArea.fadeOut();
            $('#unternehmenansicht').fadeOut();
            contentArea.trigger('reset');
            // User is logged out
        renderMessage(res.message);
        },
        error: (response) => {
            // User is still logged in
            loginForm.fadeOut();
            logoutForm.fadeIn();
        },
    });
}
