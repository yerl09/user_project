import * as express from "express"
import mysql = require("mysql");
import {MysqlError} from "mysql";
import {Request, Response} from "express";
import session= require("express-session");
import { Configuration } from '../config/config';


class User {
    public vorname: string;
    public nachname: string;
    public adresse: string;
    public plz: number;
    public email: string;
    public passwort: string;
    public anrede: string;
    id: number;
    public typ: number;

    constructor (anrede: string, id: number, vorname: string, nachname: string, adresse: string, plz: number, email: string, passwort: string, typ: number) {
        this.anrede = anrede;
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.adresse = adresse;
        this.plz = plz;
        this.email = email;
        this.passwort = passwort;
        this.typ = typ;
    }
}

// Importiere benötigte Klassen und Funktionen
const connection: mysql.Connection = mysql.createConnection({
    database: "greenhome",
    host: "localhost",
    password: "",
    user: "root",
});


const router: express.Express = express();

router.use(session(Configuration.sessionOptions));



router.use(express.urlencoded({extended: false}));
router.use(express.json());


router.listen(8080, () => {
    connection.connect(err => {
        if (!err) {
            console.log("Database connected");
        }
    });
    console.log("Server gestartet: http://localhost:8080");
});

router.use("/", express.static(__dirname + "/../../client/views"));
router.use("/src", express.static(__dirname + "/../../client/src"));
router.use("/img", express.static(__dirname + "/../../client/css/img"));
router.use("/css", express.static(__dirname + "/../../client/css"));
router.use("/bootstrap", express.static(__dirname + "/../../client/node-modules/bootstrap/dist"));
router.use("/jquery", express.static(__dirname + "/../../client/node_modules/jquery/dist"));


function isLoggedIn() {
    // Abstract middleware route for checking login state of the user
    return (req: Request, res: Response, next) => {
        if (req.session.user) {
            // User has an active session and is logged in, continue with route
            next();
        } else {
            // User is not logged in
            res.status(401).send({
                message: 'Loggen Sie sich bitte ein',
            });
        }
    };
}

router.get('/login', isLoggedIn(), (req: Request, res: Response) => {
    res.status(200).send({
        message: 'Sitzung aktiv',
        user: req.session.user,
        //userid: req.session.id,
    });
});

router.post('/logout', isLoggedIn(),(req: Request, res: Response) => {
    // Log out user
    delete req.session.user; // Delete user from session
    res.status(200).send({
        message: 'Logout war erfolgreich',
    });
});

/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
router.get('/user/:userId', isLoggedIn(), (req,res)=>{
    const query: string = 'SELECT * FROM benutzer WHERE b_id = ?;';
    const userId: number= Number(req.params.userId);

    connection.query(query, userId,(err:MysqlError, rows:any)=>{
        if (err) {
            // Database operation has failed
            res.status(500).send({
               // message: 'Database request failed: ' + err,
            });
        } else {
           if (rows.length === 1) {
               let user: User = new User(
                   rows [0].anrede,
                   rows [0].b_id,
                   rows [0].vorname,
                   rows [0].nachname,
                   rows [0].strasseundnummer,
                   rows [0].plzundort,
                   rows [0].email,
                   rows [0].passwort,
                   rows [0].typ,);

            res.status(200).send({
                message: 'Profildaten wurden geladen',
                User: user,
                anrede: rows [0].anrede,
                id: rows [0].b_id,
                vorname:rows [0].vorname,
                nachname: rows [0].nachname,
                adresse: rows [0].strasseundnummer,
                plz: rows [0].plzundort,
                email:rows [0].email,
                passwort:rows [0].passwort,
                typ: rows [0].typ,

            });
           } else {
               res.status(404).send({
                   message: 'Profildaten konnten nicht gefunden werden',
               });
           }
        }
    });
});


router.get('/users', isLoggedIn(),(req, res) => {
    // Create database query and data
    const query: string = 'SELECT * FROM benutzer;';

    // request user from database
    connection.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            // Database operation has failed
            res.status(500).send({
                //message: 'Database request failed: ' + err,
            });
        } else {
            // Create local user list to parse users from database
            const userList: User[] = [];
            // Parse every entry
            for (const row of rows) {
                userList.push(new User(
                    row.anrede,
                    row.b_id,
                    row.vorname,
                    row.nachname,
                    row.strasseundnummer,
                    row.plzundort,
                    row.email,
                    row.passwort,
                    row.typ,
                ));
            }
            // Send user list to client
            res.status(200).send({
                //message: 'Successfully requested userlist',
                userList,
            });
        }
    });
});



router.post('/create', (req: Request, res: Response) => {
    // Read data from request
    const vorname: string = req.body.vorname;
    const nachname: string = req.body.nachname;
    const adresse: string = req.body.adresse;
    const plz: number = req.body.plz;
    const email: string = req.body.email;
    const passwort: string = req.body.passwort;
    const anrede: string = req.body.anrede;
    const typ: number = req.body.typ;

    const checkEmailQuery: string = "SELECT * FROM benutzer WHERE email = ?";

    connection.query(checkEmailQuery, email, (err: MysqlError, rows: any) => {
        if (rows.length > 0) {
            res.status(400).send({
                message: "Diese E-Mail Adresse ist schon mit einem Konto verknüpft!"
            });
        } else {
            if (vorname && nachname && adresse && plz && email && passwort && anrede && typ) {
                // Create database query and data
                const data: [string, string, string, string, number, string, string, number] = [anrede, vorname, nachname, adresse, plz, email, passwort, typ]; // As standard, any new user has rights Rights.User
                const addUser: string = "INSERT INTO `benutzer`(`anrede`, `vorname`, `nachname`, `strasseundnummer`, `plzundort`, `email`, `passwort`, typ ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                // Execute database query
                connection.query(addUser, data, (err: MysqlError, result: any) => {
                    if (err) {
                        // Query could not been executed
                        res.status(500).send({
                            message: 'Anfrage kann nicht an die Datenbank gesendet werden' + err,
                        });
                    } else {
                        // The user was created
                        const getid: string = "SELECT b_id FROM benutzer WHERE email = ? ";

                        connection.query(getid, email,(err: MysqlError, result: any) => {
                            if(err) {
                                console.log(err);
                            } else {
                                const gid = result[0].b_id;
                                console.log(gid);

                                const setGroup: string = "INSERT INTO benutzerzuordnung VALUES (?, ?)";
                                const data: number[] = [typ, gid];
                                connection.query(setGroup, [gid, typ], (err: MysqlError, resu: any) => {
                                    if(err) {
                                        console.log(err);
                                    } else {
                                        console.log('gruppenzuordnung erledigt');
                                        res.status(200)
                                            .send({
                                                userId: result.insertId,
                                                message: 'Sie sind registriert',
                                            });
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                res.status(400).send({
                    message: 'Bitte füllen Sie alle restlichen Felder aus',
                });
            }
        }
    });
});





router.post('/login', (req: Request, res: Response) => {
    // Read data from request
    const email: string = req.body.email;
    const passwort: string = req.body.passwort;

    // Create database query and data
    const data: [string, string] = [email, passwort];
    const query: string = "SELECT * FROM benutzer WHERE email = ? AND passwort = ?" ;

    // request user from database
    connection.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            // Login data is incorrect, user is not logged in
            res.status(500).send({
                message: 'Database error:' + err,
            });
        } else {
            // Check if database response contains exactly one entry
            if (rows.length === 1) {
                // Login data is correct, user is logged in
                const user: User = new User(
                    rows [0].anrede,
                    rows [0].b_id,
                    rows [0].vorname,
                    rows [0].nachname,
                    rows [0].strasseundnummer,
                    rows [0].plzundort,
                    rows [0].email,
                    rows [0].passwort,
                    rows [0].typ,
                );
                req.session.user = user; // Store user object in session for authentication
                res.status(200).send({
                    message: 'Login war erfolgreich',
                    user,
                });
            } else {
                // Login data is incorrect, user is not logged in
                res.status(401).send({
                    message: 'Email und Passwort stimmen nicht überein',
                });
            }
        }
    });
});

router.put('/user/:userId', isLoggedIn(),(req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    const vorname: string = req.body.vorname;
    const nachname: string = req.body.nachname;
    const adresse: string = req.body.adresse;
    const plz: string = req.body.plz;
    const password: string = req.body.passwort;

    console.log("öööööööööö"+vorname);
    // Define data for database query
    let data: any;
    let query: string;

    // Check that all arguments are given
    if ( vorname && nachname && adresse && plz) {
        // check if password was provided

            data = [vorname, nachname, adresse, plz, userId];
            query = 'UPDATE benutzer SET vorname = ?, nachname = ?, strasseundnummer=?, plzundort=? WHERE benutzer.b_id = ?';
        //}
        // Execute database query
        connection.query(query, data, (err: MysqlError, result: any) => {
            if (err) {
                // Query could not been executed
                res.status(500).send({
                    message: 'Database request failed: ' + err,
                });
            } else {
                if (result.affectedRows === 1) {
                    // The user was updated
                    res.status(200).send({
                        message: 'Profildaten aktualisiert ' + userId,

                    });
                } else {
                    // The user can not be found
                    res.status(404).send({
                        message: 'Der Benutzer kann nicht gefunden werden',
                    });
                }
            }
        });
    } else {
        res.status(400).send({
            message: 'Bitte füllen Sie die Felder aus',
        });
    }
});

router.delete('/user/:userId', isLoggedIn(), (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    // Create database query and data
    const data: number = userId;
    const query: string = 'DELETE FROM benutzer WHERE b_id = ?;';

    // request user from database
    connection.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            // Database operation has failed
            res.status(500).send({
                message: 'Database request failed: ' + err,
            });
        } else {
            // Check if database response contains at least one entry
            if (result.affectedRows === 1) {
                res.status(200).send({
                    message: 'Successfully deleted user ' + userId,
                });
            } else {
                // No user found to delete
                res.status(404).send({
                    message: 'The requested user can not be deleted.',
                });
            }
        }
    });
});