-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 17. Jan 2020 um 13:00
-- Server-Version: 10.1.36-MariaDB
-- PHP-Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `greenhome`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `angebot`
--

CREATE TABLE `angebot` (
  `k_id` int(35) NOT NULL,
  `preis` int(35) NOT NULL,
  `auftr_id` int(35) NOT NULL,
  `b_id` int(35) NOT NULL,
  `ang_id` int(35) NOT NULL,
  `u_id` int(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `auftrag`
--

CREATE TABLE `auftrag` (
  `b_id` int(35) NOT NULL,
  `u_id` int(35) NOT NULL,
  `auftr_id` int(35) NOT NULL,
  `beschreibung` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `auftragsposten`
--

CREATE TABLE `auftragsposten` (
  `auftrpst_id` int(35) NOT NULL,
  `auftr_id` int(35) NOT NULL,
  `ang_id` int(35) NOT NULL,
  `status` varchar(255) NOT NULL,
  `bewertung` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE `benutzer` (
  `anrede` varchar(35) NOT NULL,
  `vorname` varchar(35) NOT NULL,
  `nachname` varchar(35) NOT NULL,
  `strasseundnummer` varchar(35) NOT NULL,
  `plzundort` varchar(35) NOT NULL,
  `bild` varchar(35) NOT NULL,
  `email` varchar(35) NOT NULL,
  `passwort` varchar(35) NOT NULL,
  `b_id` int(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `benutzer`
--

INSERT INTO `benutzer` (`anrede`, `vorname`, `nachname`, `strasseundnummer`, `plzundort`, `bild`, `email`, `passwort`, `b_id`) VALUES
('Frau', 'nadine', 'müller', 'mxvmndm 67', '57074', '', 'nadine@gmx.de', 'xx', 2),
('Frau', 'harald', 'müller', 'lknaslcnl', '57074', '', 'harald@gmx.de', 'yyy', 16),
('Frau', 'glodko', 'kklfv', 'dsvds', '45643334', '', 'ldgll@jngjfd.de', 'sss', 17),
('undefined', 'kndslgnvds', 'sfdac', 'Untere Dorfstraße 163', '57074', '', 'bhbb@gmail.de', 'ccc', 18),
('undefined', 'adnlsf', 'etewe', 'qfcasvas', '57074', '', 'tkbvkmfd@jvn.de', 'yyy', 19),
('Frau', 'hekfv', 'ködkd', 'jdfvöds', '42533', '', 'faskfmnas@gkpdsg', 'bbb', 20),
('Frau', 'müller', 'erol', 'Untere Dorfstraße 163', '57074', '', 'ldgll@jngjfd.dexxxx', 'xxx', 21),
('Herr', 'jbkdsvbwedjs', 'edfd', 'dfvvds', '235', '', 'rgekg.re@fgnrlkng', '111', 22),
('undefined', 'dddd', 'dddd', 'dddddd', '3333', '', 'ddd@dd', 'yyy', 23),
('undefined', 'okk', 'm,m', 'Untere Dorfstraße 163', '57074', '', 'sdsa@mdv', 'yyy', 24),
('undefined', 'harald', 'yxy', 'Untere Dorfstraße 163', '57074', '', 'yxy@sjncks', 'yyy', 25),
('Frau', 'xcxyc', 'dcdsdsdsv', 'Untere Dorfstraße 163', '57074', '', 'kmdlvkd@xn--ndaaaa', 'ccc', 26),
('undefined', 'y', 'dsvs', 'y', '57074', '', 'tkbvkmfd@jvn.deyy', 'yyy', 27),
('undefined', 'xcdsmc', 'dscd', 'dc,ldsl', '0343', '', 'mcklsd@xn--ds-gkaa', 'yyy', 28),
('Herr', 'lmsdkmsa', 'dfmmeds', 'dsflmöds', '32423', '', 'ldmsfsd@xn--dsfpksd-q2a', 'yyy', 29),
('undefined', 'glodko', 'ösalcd', 'dkmlcds', '342423', '', 'dsmlf@dsfds', 'yyy', 30),
('Frau', 'glodko', 'sfdac', 'ömsam', '57074', '', 'mdklcfds@dcnkds', 'yyy', 31),
('Frau', 'nadine', 'kgldknds', 'sakflksjajf', '2424', '', 'ufguds@xn--ndaaaa', 'yyy', 32),
('undefined', 'alis', 'ckar', 'lndkds', '223534', '', 'aaaaa@aaa', 'aaa', 33),
('Frau', 'bla', 'bla', 'sassf', '1111', '', 'jkbsjfks@jdjsjd', 'xxx', 34),
('Herr', 'sdasasd', 'hshd', 'asdhdskaj', '66363', '', 'skhdkjsa@bhsd', 'xxx', 35),
('Frau', 'sgkakjsa', 'aa', 'aaa', '111', '', 'sfjsj@jjjj', 'xxx', 36),
('Herr', 'bsamdsan', 'wwww', 'wwww', '22222', '', 'jhdf@bhsbd', 'aaa', 37),
('Frau', 'fff', 'ffff', 'ffff', '111', '', 'dkfnn@kkk', 'yyy', 38);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzergruppen`
--

CREATE TABLE `benutzergruppen` (
  `b_gr_id` int(35) NOT NULL,
  `b_zu_id` int(35) NOT NULL,
  `bezeichnung` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `benutzergruppen`
--

INSERT INTO `benutzergruppen` (`b_gr_id`, `b_zu_id`, `bezeichnung`) VALUES
(1, 1, 'Admin'),
(2, 2, 'Unternehmen'),
(3, 3, 'Kunde');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzerzuordnung`
--

CREATE TABLE `benutzerzuordnung` (
  `b_id` int(35) NOT NULL,
  `b_gr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `benutzerzuordnung`
--

INSERT INTO `benutzerzuordnung` (`b_id`, `b_gr_id`) VALUES
(38, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kategorie`
--

CREATE TABLE `kategorie` (
  `k_id` int(35) NOT NULL,
  `beschreibung` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nachricht`
--

CREATE TABLE `nachricht` (
  `na_id` int(35) NOT NULL,
  `sen_id` int(35) NOT NULL,
  `empf_id` int(35) NOT NULL,
  `inhalt` varchar(255) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `unternehmensprofil`
--

CREATE TABLE `unternehmensprofil` (
  `u_id` int(35) NOT NULL,
  `b_id` int(35) NOT NULL,
  `u_beschreibung` varchar(255) NOT NULL,
  `preis` int(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `angebot`
--
ALTER TABLE `angebot`
  ADD PRIMARY KEY (`ang_id`),
  ADD KEY `k_id` (`k_id`),
  ADD KEY `auftr_id` (`auftr_id`),
  ADD KEY `b_id` (`b_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indizes für die Tabelle `auftrag`
--
ALTER TABLE `auftrag`
  ADD PRIMARY KEY (`auftr_id`),
  ADD KEY `b_id` (`b_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indizes für die Tabelle `auftragsposten`
--
ALTER TABLE `auftragsposten`
  ADD PRIMARY KEY (`auftrpst_id`),
  ADD KEY `auftr_id` (`auftr_id`),
  ADD KEY `ang_id` (`ang_id`);

--
-- Indizes für die Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  ADD PRIMARY KEY (`b_id`);

--
-- Indizes für die Tabelle `benutzergruppen`
--
ALTER TABLE `benutzergruppen`
  ADD PRIMARY KEY (`b_gr_id`),
  ADD KEY `b_zu_id` (`b_zu_id`);

--
-- Indizes für die Tabelle `benutzerzuordnung`
--
ALTER TABLE `benutzerzuordnung`
  ADD KEY `b_id` (`b_id`),
  ADD KEY `b_gr_id` (`b_gr_id`);

--
-- Indizes für die Tabelle `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`k_id`);

--
-- Indizes für die Tabelle `nachricht`
--
ALTER TABLE `nachricht`
  ADD PRIMARY KEY (`na_id`),
  ADD KEY `sen_id` (`sen_id`),
  ADD KEY `empf_id` (`empf_id`);

--
-- Indizes für die Tabelle `unternehmensprofil`
--
ALTER TABLE `unternehmensprofil`
  ADD PRIMARY KEY (`u_id`),
  ADD KEY `b_id` (`b_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  MODIFY `b_id` int(35) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `angebot`
--
ALTER TABLE `angebot`
  ADD CONSTRAINT `angebot_ibfk_1` FOREIGN KEY (`k_id`) REFERENCES `kategorie` (`k_id`),
  ADD CONSTRAINT `angebot_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `unternehmensprofil` (`u_id`);

--
-- Constraints der Tabelle `auftrag`
--
ALTER TABLE `auftrag`
  ADD CONSTRAINT `auftrag_ibfk_1` FOREIGN KEY (`b_id`) REFERENCES `benutzer` (`b_id`),
  ADD CONSTRAINT `auftrag_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `unternehmensprofil` (`u_id`);

--
-- Constraints der Tabelle `auftragsposten`
--
ALTER TABLE `auftragsposten`
  ADD CONSTRAINT `auftragsposten_ibfk_1` FOREIGN KEY (`auftr_id`) REFERENCES `auftrag` (`auftr_id`),
  ADD CONSTRAINT `auftragsposten_ibfk_2` FOREIGN KEY (`ang_id`) REFERENCES `angebot` (`ang_id`);

--
-- Constraints der Tabelle `benutzerzuordnung`
--
ALTER TABLE `benutzerzuordnung`
  ADD CONSTRAINT `benutzerzuordnung_ibfk_1` FOREIGN KEY (`b_id`) REFERENCES `benutzer` (`b_id`),
  ADD CONSTRAINT `benutzerzuordnung_ibfk_2` FOREIGN KEY (`b_gr_id`) REFERENCES `benutzergruppen` (`b_gr_id`);

--
-- Constraints der Tabelle `nachricht`
--
ALTER TABLE `nachricht`
  ADD CONSTRAINT `nachricht_ibfk_1` FOREIGN KEY (`sen_id`) REFERENCES `benutzer` (`b_id`),
  ADD CONSTRAINT `nachricht_ibfk_2` FOREIGN KEY (`empf_id`) REFERENCES `benutzer` (`b_id`);

--
-- Constraints der Tabelle `unternehmensprofil`
--
ALTER TABLE `unternehmensprofil`
  ADD CONSTRAINT `unternehmensprofil_ibfk_1` FOREIGN KEY (`b_id`) REFERENCES `benutzer` (`b_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
